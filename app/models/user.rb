class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :products

  def admin?
    self.role == 1
  end

  def provider?
    self.role == 2
  end

  def client?
    self.role == 3
  end
end
